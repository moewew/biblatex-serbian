#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import re
from functools import reduce
from typing import Callable
from typing import Dict
from typing import Iterable
from typing import List
from typing import Optional
from typing import Pattern
from typing import Tuple
from typing import TypeVar
from typing import Union

import yaml
from jinja2 import Environment
from jinja2 import FileSystemLoader
from srtools import cyrillic_to_latin
from srtools import latin_to_cyrillic

from utf8_to_licr import translation_table as licr_translation_dict

BibStrEntryDict = Dict[str, Union[str, List[str]]]


def create_latex_env(
    loc_string_funcion: Callable[[str, Optional[str]], str]
) -> Environment:
    latex_env = Environment(
        block_start_string=r"\BLOCK{",
        block_end_string=r"}",
        variable_start_string=r"\VAR{",
        variable_end_string=r"}",
        comment_start_string=r"\#{",
        comment_end_string=r"}",
        line_statement_prefix=r"%%!",
        line_comment_prefix=r"%#",
        trim_blocks=True,
        lstrip_blocks=True,
        autoescape=False,
        loader=FileSystemLoader(os.path.abspath("templates")),
    )

    def bibstring(entry: Union[str, List[str]]) -> str:
        return generate_bib_string(entry, loc_string_funcion)

    latex_env.filters["bibstring"] = bibstring
    return latex_env


def generate_bib_string(
    entry: Union[str, List[str]],
    loc_string_funcion: Callable[[str, Optional[str]], str] = None,
) -> str:
    def process_loc_str(entry: Union[str, Dict[str, str]]) -> str:
        if isinstance(entry, str):
            if loc_string_funcion:
                entry = loc_string_funcion(entry, None)
            return entry
        elif isinstance(entry, dict):
            assert len(entry) == 1
            key = next(iter(entry))
            text = entry[key]
            if loc_string_funcion:
                text = loc_string_funcion(text, key)
            return text
        else:
            raise TypeError("Invalid type")

    if isinstance(entry, list):
        long, short = entry
        long, short = map(process_loc_str, [long, short])
    elif isinstance(entry, (str, dict)):
        long = short = process_loc_str(entry)
    else:
        raise TypeError(f"Input must be `str` or List[`str`].")

    return f"{{{{{long}}}{{{short}}}}}"


def render_biblatex(
    language_name: str,
    bibliography_loclization_entries: BibStrEntryDict,
    loc_string_funcion: Callable[[str, Optional[str]], str],
) -> str:
    latex_env = create_latex_env(loc_string_funcion)
    template = latex_env.get_template("common.lbx")
    rendered_string = template.render(**locals())
    return rendered_string


def unicode_to_licr(text: str) -> str:
    return text.translate(licr_translation_dict)


_interp_whitesp_to_macro_rx_repl_pairs = [
    (re.compile(r"\.(?:\\)? "), r"\\adddot\ "),
    (re.compile(r"\.\s*$"), r"\\adddot"),
    (re.compile(r"\.`"), r"\\adddotspace "),
    (re.compile(r"`"), r"\\addabbrvspace "),
    (re.compile(r"~"), r"\\addnbspace "),
    (re.compile(r"/"), r"\\addslash "),
    (re.compile(r":"), r"\\addcolon"),
]

T = TypeVar("T")


def compose_functions_iter(
    functions: Iterable[Callable[[T], T]]
) -> Callable[[T], T]:
    func_list = list(functions)

    def new_function(argument: T) -> T:
        return reduce(lambda r, f: f(r), func_list, argument)

    return new_function


def compose_functions(*args: Callable[[T], T]) -> Callable[[T], T]:
    return compose_functions_iter(args)


def interp_whitesp_to_macro(text: str) -> str:
    def _apply_rx_sub(text: str, rx_sub: Tuple[Pattern[str], str]) -> str:
        rx, sub = rx_sub
        return rx.sub(sub, text)

    return reduce(_apply_rx_sub, _interp_whitesp_to_macro_rx_repl_pairs, text)


_space_macro_rx = re.compile(r"\\\w+")


def space_macro_fixup(text: str) -> str:
    return _space_macro_rx.sub(lambda l: cyrillic_to_latin(l.group()), text)


def main():
    with open(
        "serbian_latin_loc_strings.yaml", "r", encoding="utf-8"
    ) as yamlfile, open(
        "serbian.lbx", "w", encoding="ascii"
    ) as latin_lbx_file, open(
        "serbianc.lbx", "w", encoding="utf-8"
    ) as cyrillic_lbx_file:
        latin_localization_entries: BibStrEntryDict = yaml.load(
            yamlfile, Loader=yaml.FullLoader
        )
        latin_lbx_file.write(
            render_biblatex(
                "serbian",
                latin_localization_entries,
                lambda t, _: compose_functions(
                    interp_whitesp_to_macro, unicode_to_licr
                )(t),
            )
        )

        def cyrillic_proc(text: str, option: str) -> str:
            if option == "latin_only":
                return interp_whitesp_to_macro(text)

            return space_macro_fixup(
                latin_to_cyrillic(interp_whitesp_to_macro(text))
            )

        cyrillic_lbx_file.write(
            render_biblatex(
                "serbianc", latin_localization_entries, cyrillic_proc
            )
        )


if __name__ == "__main__":
    main()
